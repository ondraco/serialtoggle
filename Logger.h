#ifdef logSerial
#define log log_x
#define logln logln_x
#define initLog initLog_x
#else
#define log
#define logln
#define initLog
#endif

namespace Logger
{
#ifdef logSerial
    void log_x(const char *msg)
    {
        logSerial.print(msg);
#ifdef cardSerial
        cardSerial.print(msg);
#endif
    }

    void log_x(const String msg)
    {
        logSerial.print(msg);
#ifdef cardSerial
        cardSerial.print(msg);
#endif
    }

    void logln_x(const String msg)
    {
        logSerial.println(msg);
#ifdef cardSerial
        cardSerial.println(msg);
#endif
    }

    void logln_x(const char *msg)
    {
        logSerial.println(msg);
#ifdef cardSerial
        cardSerial.println(msg);
#endif
    }

    void initLog_x(int baudRate = 115200, int cardSerialBaud = 115200)
    {
        logSerial.begin(baudRate);

        while (!logSerial)
            delay(100);

#ifdef cardSerial
        cardSerial.begin(cardSerialBaud);

        while (!cardSerial)
            delay(100);
#endif
    }
#endif //logSerial
} // namespace Logger